import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChaineComponent } from './chaine/chaine.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { HomeComponent } from './home/home.component';
const routes: Routes = [
 
  { path: '', component: HomeComponent },
  { path: 'chaine', component: ChaineComponent }
];
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
