import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddchaineComponent } from './addchaine.component';

describe('AddchaineComponent', () => {
  let component: AddchaineComponent;
  let fixture: ComponentFixture<AddchaineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddchaineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddchaineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
